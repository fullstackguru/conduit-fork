import ReactDOM from 'react-dom';
import React from 'react';
import {Provider} from 'react-redux';
import {HashRouter, Route, Switch} from 'react-router-dom'
import Article from './components/Article';
import Editor from './components/Editor';
import Header from './components/Header';
import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register';
import Settings from './components/Settings';
import Profile from './components/Profile';
import store from './store';

ReactDOM.render((
    <Provider store={store}>
      <HashRouter>
        <div>
          <Header/>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Register}/>
            <Route path="/settings" component={Settings}/>
            <Route path="/article/:id"
                   render={(props) => (
                <Article {...this.props} {...props} />
            )}/>
            <Route path="/@:username"
                   render={(props) => (
                       <Profile {...this.props} {...props} />
                   )}/>
            <Route path="/editor" component={Editor} />
            <Route path="/editor/:slug"
                   render={(props) => (
                       <Editor {...this.props} {...props} />
                   )}/>
          </Switch>
          {/*<Route path="/" component={App}>

           </Route>*/}
        </div>
      </HashRouter>
    </Provider>
), document.getElementById('root'));
