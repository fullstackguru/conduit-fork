import {Link} from 'react-router-dom'
import React from 'react';
import agent from '../agent';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

const mapStateToProps = state => ({
  appLoaded: state.common.appLoaded,
  appName: state.common.appName,
  currentUser: state.common.currentUser,
  redirectTo: state.common.redirectTo
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) =>
      dispatch({ type: 'APP_LOAD', payload, token }),
  onRedirect: () =>
      dispatch({ type: 'REDIRECT' })
});

const LoggedOutView = props => {
  if (!props.currentUser) {
    return (
        <ul className="nav navbar-nav pull-xs-right">

          <li className="nav-item">
            <Link to="/" className="nav-link">
              Home
            </Link>
          </li>

          <li className="nav-item">
            <Link to="login" className="nav-link">
              Sign in
            </Link>
          </li>

          <li className="nav-item">
            <Link to="register" className="nav-link">
              Sign up
            </Link>
          </li>

        </ul>
    );
  }
  return null;
};

const LoggedInView = props => {
  if (props.currentUser) {
    return (
        <ul className="nav navbar-nav pull-xs-right">

          <li className="nav-item">
            <Link to="" className="nav-link">
              Home
            </Link>
          </li>

          <li className="nav-item">
            <Link to="editor" className="nav-link">
              <i className="ion-compose"></i>&nbsp;New Post
            </Link>
          </li>

          <li className="nav-item">
            <Link to="settings" className="nav-link">
              <i className="ion-gear-a"></i>&nbsp;Settings
            </Link>
          </li>

          <li className="nav-item">
            <Link
                to={`@${props.currentUser.username}`}
                className="nav-link">
              <img src={props.currentUser.image} className="user-pic" />
              {props.currentUser.username}
            </Link>
          </li>

        </ul>
    );
  }

  return null;
};

class Header extends React.Component {
  componentWillMount() {
    const token = window.localStorage.getItem('jwt');
    if (token) {
      agent.setToken(token);
    }

    this.props.onLoad(token ? agent.Auth.current() : null, token);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      this.context.router.history.replace(nextProps.redirectTo);
      this.props.onRedirect();
    }
  }

  render() {
    if (this.props.appLoaded) {
      return (
          <nav className="navbar navbar-light">
            <div className="container">
              <Link to="/" className="navbar-brand">
                {this.props.appName.toLowerCase()}
              </Link>
              <LoggedOutView currentUser={this.props.currentUser}/>
              <LoggedInView currentUser={this.props.currentUser}/>
            </div>
          </nav>
      )
    }
    return (
        <nav className="navbar navbar-light">
          <div className="container">
            <Link to="/" className="navbar-brand">
              {this.props.appName.toLowerCase()}
            </Link>
            <LoggedOutView currentUser={this.props.currentUser}/>
            <LoggedInView currentUser={this.props.currentUser}/>
          </div>
        </nav>
    )
  }
}

Header.contextTypes = {
  router: PropTypes.func.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(Header)