import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';

const mapStateToProps = state => ({
  ...state.editor
});

/**
 * `mapDispatchToProps()` needs separate actions for adding/removing
 * tags, submitting an article, updating individual fields, and cleaning
 * up after navigating away from the page.
 */
const mapDispatchToProps = dispatch => ({
  onAddTag: () =>
      dispatch({ type: 'ADD_TAG' }),
  onLoad: payload =>
      dispatch({ type: 'EDITOR_PAGE_LOADED', payload }),
  onRemoveTag: tag =>
      dispatch({ type: 'REMOVE_TAG', tag }),
  onSubmit: payload =>
      dispatch({ type: 'ARTICLE_SUBMITTED', payload }),
  onUnload: payload =>
      dispatch({ type: 'EDITOR_PAGE_UNLOADED' }),
  onUpdateField: (key, value) =>
      dispatch({ type: 'UPDATE_FIELD_EDITOR', key, value })
});

class Editor extends React.Component {
  render() {
    return (
        <div className="editor-page">
          <div className="container page">
            <div className="row">
              <div className="col-md-10 offset-md-1 col-xs-12">

                <ListErrors errors={this.props.errors}></ListErrors>

                <form>
                  <fieldset>

                    <fieldset className="form-group">
                      <input
                          className="form-control form-control-lg"
                          type="text"
                          placeholder="Article Title"
                          value={this.props.title}
                          onChange={this.changeTitle} />
                    </fieldset>

                    <fieldset className="form-group">
                      <input
                          className="form-control"
                          type="text"
                          placeholder="What's this article about?"
                          value={this.props.description}
                          onChange={this.changeDescription} />
                    </fieldset>

                    <fieldset className="form-group">
                    <textarea
                        className="form-control"
                        rows="8"
                        placeholder="Write your article (in markdown)"
                        value={this.props.body}
                        onChange={this.changeBody}>
                    </textarea>
                    </fieldset>

                    <fieldset className="form-group">
                      <input
                          className="form-control"
                          type="text"
                          placeholder="Enter tags"
                          value={this.props.tagInput}
                          onChange={this.changeTagInput}
                          onKeyUp={this.watchForEnter} />

                      <div className="tag-list">
                        {
                          (this.props.tagList || []).map(tag => {
                            return (
                                <span className="tag-default tag-pill" key={tag}>
                              <i  className="ion-close-round"
                                  onClick={this.removeTagHandler(tag)}>
                              </i>
                                  {tag}
                            </span>
                            );
                          })
                        }
                      </div>
                    </fieldset>

                    <button
                        className="btn btn-lg pull-xs-right btn-primary"
                        type="button"
                        disabled={this.props.inProgress}
                        onClick={this.submitForm}>
                      Publish Article
                    </button>

                  </fieldset>
                </form>

              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor);