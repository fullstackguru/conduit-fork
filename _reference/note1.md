`create-react-app xxx`

```
npm start:      Starts the development server.
npm run build : Bundles the app into static files for production.
npm test :      Starts the test runner.
npm run eject : Removes this tool and copies build dependencies, configuration files
                and scripts into the app directory. If you do this, you can’t go back!
```

> For Redux implementation

* Reducers should not modify or rely on any global state
* Store has three functions
```
1. getState     : fetches the current state of a store
2. dispatch     : fires off a new action
3. subscribe    : fires a callback everytime there's a new action after a reducer's action
```

```
We'll use mapStateToProps() to access our specific state we want and pass it to props.
Because we're using React Redux, we no longer need componentWillMount() or the constructor.
Then we use the connect() function to bind our state to the App component.
```

* Dispatch
```
This mapDispatchToProps function is the second function that gets passed to connect().
This function maps the Redux store's dispatch() to other functions.
Each function that mapDispatchToProps() returns gets attached to the component's props.
This means your component can call this.props.onLoad() to fire off an event with type 'HOME_PAGE_LOADED'
and a 'payload', which is the Promise from our request.
```

__Note__

export and import with brackets: https://stackoverflow.com/questions/31096597/using-brackets-with-javascript-import-syntax

jsx spread attributes: (...state) https://stackoverflow.com/questions/31048953/what-do-these-three-dots-in-react-do

import React from 'react': __here `React` means namespace__
import {Component} from 'react': __here `Component` means member__

* App.contextTypes
App.contextTypes snippet tells react-router to attach the `children` property
to this component's props

* React Router

After version 4, hashHistory and browserHistory moved to react-router-dom package
and IndexRoute is deprecated.

```
Links
Finally, our application needs a way to navigate between pages.
If we were to create links using anchor elements, clicking on them
would cause the whole page to reload.
React Router provides a <Link> component to prevent that from happening.
When clicking a <Link>, the URL will be updated and the rendered content
will change without reloading the page.
```